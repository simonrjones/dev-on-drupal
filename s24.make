; Studio 24 Drupal 7 make file
core = 7.x
api = 2
projects[drupal][version] = "7.34"

; Modules
projects[admin_menu][subdir] = "contrib"
projects[admin_menu][version] = "3"

projects[adminimal_admin_menu][subdir] = "contrib"
projects[adminimal_admin_menu][version] = "1.5"

projects[advanced_help][subdir] = "contrib"
projects[advanced_help][version] = "1.1"

projects[ctools][subdir] = "contrib"
projects[ctools][version] = "1.5"

projects[module_filter][subdir] = "contrib"
projects[module_filter][version] = "2.0-alpha2"

projects[views][subdir] = "contrib"
projects[views][version] = "3.8"

projects[context][subdir] = "contrib"
projects[context][version] = "3.5"

projects[features][subdir] = "contrib"
projects[features][version] = "2.3"

projects[ftools][subdir] = "contrib"
projects[ftools][version] = "1.6"

projects[strongarm][subdir] = "contrib"
projects[strongarm][version] = "2.0"

projects[features_extra][subdir] = "contrib"
projects[features_extra][version] = "1.0"

projects[block_class][subdir] = "contrib"
projects[block_class][version] = "2.1"

; Themes
projects[omega][version] = "4.3"




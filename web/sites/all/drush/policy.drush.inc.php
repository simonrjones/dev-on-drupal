<?php

function drush_policy_sql_sync_validate($source = null, $destination = null) {
    $liveEnv = array(
        '@prod',
        '@production'
    );
    if (in_array($destination, $liveEnv)) {
        return drush_set_error("That was a close call");
    }
}
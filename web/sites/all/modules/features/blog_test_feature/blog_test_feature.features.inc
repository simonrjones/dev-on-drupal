<?php
/**
 * @file
 * blog_test_feature.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function blog_test_feature_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function blog_test_feature_node_info() {
  $items = array(
    'blog_post' => array(
      'name' => t('Blog post'),
      'base' => 'node_content',
      'description' => t('A blog article to be added to the blog section'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Some help text for the title'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
